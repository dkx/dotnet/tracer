# DKX.Tracer

Tracing library

## Installation

```bash
$ dotnet add package DKX.Tracer
```

## Exporters

* `DKX.Tracer.Exporters.VoidExporter`: exports nowhere
* `DKX.Tracer.Exporters.NonWaitingExporter`: exporter wrapper which does not wait for the result of the underlying exporter
* `DKX.TracerGoogleCloud.GoogleCloudExporter`: exports to Google Cloud Trace, must install package `DKX.TracerGoogleCloud`

## Usage

```c#
using DKX.Tracer;
using DKX.Tracer.Exporters;

var exporter = new VoidExporter();
var tracer = new Tracer(exporter);

await using (var rootSpan = tracer.Start("Root"))
{
    using (var child = rootSpan.fork("Child 1"))
    {
        // todo: do something
    }
}
```

Of course you can nest spans as much as you want.

## ASP.NET

There is middleware which will automatically wrap everything in a span.

```c#
using DKX.Tracer.AspNet;

public void Configure(IApplicationBuilder app)
{
    // tracer should be the first middleware so it can wrap really everything
    app.useDkxTracer();

    app.UseRouting();
    app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
}
``` 

## Current span provider

You can use `ICurrentSpanProvider` to get the currently opened span.

*It's better to first try to pass the span manually.*

```c#
using DKX.Tracer;

ICurrentSpanProvider currentSpanProvider = tracer.CurrentSpanProvider;
ISpan? currentSpan = currentSpanProvider.CurrentSpan;
```

## Attach additional information

Custom data can be added with `AddAttribute` methods on any span.

```c#
span.AddAttribute("key", "value");
```

It is also possible to attach `Exception`, `HttpRequest` and `HttpResponse` directly to root span to enrich the data set 
automatically.

**Provided `TracerMiddleware` is doing that automatically.**

```c#
rootSpan.AttachHttpRequest(httpRequest);
rootSpan.AttachHttpResponse(httpResponse);

try {
    // todo: process http request 
} catch (Exception e) {
    rootSpan.AttachException(e);
    throw;
}
```
