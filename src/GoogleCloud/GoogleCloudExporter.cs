using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DKX.Tracer;
using DKX.Tracer.Attributes;
using DKX.Tracer.Exporters;
using Google.Cloud.Trace.V2;
using Google.Protobuf.WellKnownTypes;
using Microsoft.AspNetCore.Http.Extensions;

namespace DKX.TracerGoogleCloud
{
	public class GoogleCloudExporter : IExporter
	{
		private readonly TraceServiceClient _client;
		
		private readonly string _projectId;

		private readonly string? _projectName;

		private readonly string? _projectVersion;

		public GoogleCloudExporter(TraceServiceClient client, string projectId, string? projectName = null, string? projectVersion = null)
		{
			_client = client;
			_projectId = projectId;
			_projectName = projectName;
			_projectVersion = projectVersion;
		}

		public Task Save(IRootSpan span, CancellationToken cancellationToken)
		{
			AddBaseAttributes(span);

			var googleSpans = ToGoogleSpans(span, CreateId(32));

			return _client.BatchWriteSpansAsync(
				$"projects/{_projectId}",
				googleSpans,
				cancellationToken
			);
		}

		private IEnumerable<Google.Cloud.Trace.V2.Span> ToGoogleSpans(ISpan span, string traceId, string? parentSpanId = null)
		{
			if (!span.Closed)
			{
				throw new Exception($"Can not export span {span.Name}, it is not closed");
			}

			var result = new List<Google.Cloud.Trace.V2.Span>();
			var googleSpan = new Google.Cloud.Trace.V2.Span();
			var id = CreateId(16);

			googleSpan.Name = CreateSpanName(traceId, id);
			googleSpan.SpanId = id;
			googleSpan.DisplayName = new TruncatableString {Value = span.Name};
			googleSpan.StartTime = Timestamp.FromDateTime(span.StartedAt);
			googleSpan.EndTime = Timestamp.FromDateTime((DateTime) span.ClosedAt!);
			googleSpan.ChildSpanCount = span.Children.Count;

			if (parentSpanId != null)
			{
				googleSpan.ParentSpanId = parentSpanId;
				googleSpan.SameProcessAsParentSpan = true;
			}

			if (span.Attributes.Count > 0)
			{
				googleSpan.Attributes = new Google.Cloud.Trace.V2.Span.Types.Attributes();
				
				foreach (var attribute in span.Attributes)
				{
					var value = new AttributeValue();

					switch (attribute)
					{
						case StringAttribute attr: value.StringValue = new TruncatableString {Value = attr.Value}; break;
						case NumberAttribute attr: value.IntValue = attr.Value; break;
						case BooleanAttribute attr: value.BoolValue = attr.Value; break;
					}

					googleSpan.Attributes.AttributeMap.Add(attribute.Name, value);
				}
			}

			result.Add(googleSpan);

			if (span.Children.Count > 0)
			{
				foreach (var child in span.Children)
				{
					result.AddRange(ToGoogleSpans(child, traceId, id));
				}
			}

			return result;
		}

		private string CreateSpanName(string traceId, string spanId)
		{
			return $"projects/{_projectId}/traces/{traceId}/spans/{spanId}";
		}

		private static string CreateId(int length)
		{
			const string valid = "0123456789ABCDEF";
			StringBuilder res = new StringBuilder();
			var rnd = new Random();
			byte[] uintBuffer = new byte[sizeof(uint)];

			while (length-- > 0)
			{
				rnd.NextBytes(uintBuffer);
				var num = BitConverter.ToUInt32(uintBuffer, 0);
				res.Append(valid[(int) (num % (uint) valid.Length)]);
			}

			return res.ToString();
		}

		private void AddBaseAttributes(IRootSpan span)
		{
			if (_projectName != null)
			{
				span.AddAttribute("g.co/gae/app/module", _projectName);
			}

			if (_projectVersion != null)
			{
				span.AddAttribute("g.co/gae/app/version", _projectVersion);
			}

			if (span.HttpRequest != null)
			{
				span.AddAttribute("/http/method", span.HttpRequest.Method);
				span.AddAttribute("/http/host", span.HttpRequest.Host.Value);
				span.AddAttribute("/http/path", span.HttpRequest.Path.Value);
				span.AddAttribute("/http/url", span.HttpRequest.GetDisplayUrl());

				try
				{
					span.AddAttribute("/http/request/size", span.HttpRequest.Body.Length);
				}
				catch (Exception)
				{
					// ignored
				}

				if (span.HttpRequest.Headers.ContainsKey("user-agent"))
				{
					span.AddAttribute("/http/user_agent", span.HttpRequest.Headers["user-agent"].ToString());
				}
			}

			if (span.HttpResponse != null)
			{
				span.AddAttribute("/http/status_code", span.HttpResponse.StatusCode);

				try
				{
					span.AddAttribute("/http/response/size", span.HttpResponse.Body.Length);
				}
				catch (Exception)
				{
					// ignored
				}
			}

			if (span.Exception != null)
			{
				var type = span.Exception.GetType();
				var statusCode = span.HttpRequest?.Method.ToUpper() == "OPTIONS" ? 200 : 500;
			
				span.AddAttribute("/http/status_code", statusCode);
				span.AddAttribute("/error/name", (type.Namespace == null ? "" : $"{type.Namespace}.") + type.Name);
				span.AddAttribute("/error/message", span.Exception.Message);
			}
		}
	}
}
