using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;

namespace DKX.Tracer.AspNet
{
	public static class ApplicationBuilderExt
	{
		public static IApplicationBuilder useDkxTracer(this IApplicationBuilder builder)
		{
			return builder.UseMiddleware<TracerMiddleware>();
		}
	}
	
	public static class HttpContextExt
	{
		public static IRootSpan? FindTracerRootSpan(this HttpContext context)
		{
			return context.Items.ContainsKey(TracerMiddleware.RootSpanKey)
				? context.Items[TracerMiddleware.RootSpanKey] as IRootSpan
				: null;
		}
		
		public static IRootSpan GetTracerRootSpan(this HttpContext context)
		{
			var span = context.FindTracerRootSpan();
			if (span == null)
			{
				throw new Exception("There is no root span in current HttpContext");
			}

			return span;
		}
	}
}
