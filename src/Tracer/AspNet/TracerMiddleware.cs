using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace DKX.Tracer.AspNet
{
	public class TracerMiddleware
	{
		public const string RootSpanKey = "dkx.tracer.root_span";

		private readonly RequestDelegate _next;

		public TracerMiddleware(RequestDelegate next)
		{
			_next = next;
		}

		public async Task Invoke(HttpContext context, ITracer tracer)
		{
			var method = context.Request.Method.ToUpper();
			if (method == "OPTIONS")
			{
				await _next(context);
				return;
			}

			await using var span = tracer.Start(method + ":" + context.Request.Path);
			span.AttachHttpRequest(context.Request);
			span.AttachHttpResponse(context.Response);

			context.Items[RootSpanKey] = span;

			try
			{
				await _next(context);
			}
			catch (Exception e)
			{
				span.AttachException(e);
				throw;
			}
		}
	}
}