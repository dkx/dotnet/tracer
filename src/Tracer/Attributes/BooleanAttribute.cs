namespace DKX.Tracer.Attributes
{
	public class BooleanAttribute : IAttribute
	{
		public BooleanAttribute(string name, bool value)
		{
			Name = name;
			Value = value;
		}
		
		public string Name { get; }
		
		public bool Value { get; }
	}
}
