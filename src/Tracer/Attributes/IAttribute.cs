namespace DKX.Tracer.Attributes
{
	public interface IAttribute
	{
		string Name { get; }
	}
}
