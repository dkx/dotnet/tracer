namespace DKX.Tracer.Attributes
{
	public class NumberAttribute : IAttribute
	{
		public NumberAttribute(string name, long value)
		{
			Name = name;
			Value = value;
		}
		
		public string Name { get; }
		
		public long Value { get; }
	}
}
