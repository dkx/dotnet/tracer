namespace DKX.Tracer.Attributes
{
	public class StringAttribute : IAttribute
	{
		public StringAttribute(string name, string value)
		{
			Name = name;
			Value = value;
		}
		
		public string Name { get; }
		
		public string Value { get; }
	}
}
