using System;

namespace DKX.Tracer
{
	public class CurrentDateTimeProvider : IDateTimeProvider
	{
		public DateTime GetUtcNow()
		{
			return DateTime.UtcNow;
		}
	}
}
