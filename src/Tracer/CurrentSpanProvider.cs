using DKX.Tracer.Events;

namespace DKX.Tracer
{
	internal class CurrentSpanProvider : ICurrentSpanProvider
	{
		public ISpan? CurrentSpan { get; private set; }

		public void SetCurrentSpan(ISpan? span)
		{
			if (CurrentSpan != null)
			{
				CurrentSpan.OnForked -= SpanOnForked;
				CurrentSpan.OnClosed -= SpanOnClosed;
			}
			
			CurrentSpan = span;

			if (span == null)
			{
				return;
			}

			span.OnForked += SpanOnForked;
			span.OnClosed += SpanOnClosed;
		}

		private void SpanOnForked(object? sender, SpanForkedArgs args)
		{
			SetCurrentSpan(args.Child);
		}

		private void SpanOnClosed(object? sender, SpanClosedArgs args)
		{
			SetCurrentSpan(CurrentSpan?.Parent);
		}
	}
}
