using System;

namespace DKX.Tracer.Events
{
	public class SpanClosedArgs : EventArgs
	{
		public SpanClosedArgs(ISpan span)
		{
			Span = span;
		}
		
		public ISpan Span { get; }
	}
}