using System;

namespace DKX.Tracer.Events
{
	public class SpanForkedArgs : EventArgs
	{
		public SpanForkedArgs(ISpan parent, ISpan child)
		{
			Parent = parent;
			Child = child;
		}
		
		public ISpan Parent { get; }
		
		public ISpan Child { get; }
	}
}