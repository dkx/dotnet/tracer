using System.Threading;
using System.Threading.Tasks;

namespace DKX.Tracer.Exporters
{
	public interface IExporter
	{
		Task Save(IRootSpan span, CancellationToken cancellationToken);
	}
}
