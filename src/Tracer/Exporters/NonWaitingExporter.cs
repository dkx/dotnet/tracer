using System.Threading;
using System.Threading.Tasks;

namespace DKX.Tracer.Exporters
{
	public class NonWaitingExporter : IExporter
	{
		private readonly IExporter _exporter;
		
		public NonWaitingExporter(IExporter exporter)
		{
			_exporter = exporter;
		}
		
		public Task Save(IRootSpan span, CancellationToken cancellationToken)
		{
			_exporter.Save(span, cancellationToken);
			return Task.CompletedTask;
		}
	}
}