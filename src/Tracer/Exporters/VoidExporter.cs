using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace DKX.Tracer.Exporters
{
	public class VoidExporter : IExporter
	{
		public Task Save(IRootSpan span, CancellationToken cancellationToken)
		{
			return Task.CompletedTask;
		}

		public Task Save(IRootSpan span, HttpRequest request, HttpResponse response, CancellationToken cancellationToken)
		{
			return Task.CompletedTask;
		}

		public Task Save(IRootSpan span, HttpRequest request, Exception e, CancellationToken cancellationToken)
		{
			return Task.CompletedTask;
		}
	}
}
