namespace DKX.Tracer
{
	public interface ICurrentSpanProvider
	{
		ISpan? CurrentSpan { get; }
	}
}
