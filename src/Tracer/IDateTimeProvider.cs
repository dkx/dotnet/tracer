using System;

namespace DKX.Tracer
{
	public interface IDateTimeProvider
	{
		DateTime GetUtcNow();
	}
}
