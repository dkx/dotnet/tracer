using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace DKX.Tracer
{
	public interface IRootSpan : ISpan, IAsyncDisposable
	{
		HttpRequest? HttpRequest { get; }
		
		HttpResponse? HttpResponse { get; }
		
		Exception? Exception { get; }

		IRootSpan AttachHttpRequest(HttpRequest request);

		IRootSpan AttachHttpResponse(HttpResponse response);

		IRootSpan AttachException(Exception exception);

		Task Save(CancellationToken cancellationToken = default);
	}
}