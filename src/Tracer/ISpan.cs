using System;
using System.Collections.Immutable;
using DKX.Tracer.Attributes;
using DKX.Tracer.Events;

namespace DKX.Tracer
{
	public interface ISpan : IDisposable
	{
		event EventHandler<SpanForkedArgs>? OnForked; 
		
		event EventHandler<SpanClosedArgs>? OnClosed;

		string Name { get; }

		DateTime StartedAt { get; }

		DateTime? ClosedAt { get; }

		ISpan? Parent { get; }
		
		IImmutableList<ISpan> Children { get; }
		
		IImmutableList<IAttribute> Attributes { get; }
		
		bool Closed { get; }

		ISpan AddAttribute(string name, string value);

		ISpan AddAttribute(string name, long value);

		ISpan AddAttribute(string name, bool value);

		ISpan Fork(string name);
	}
}
