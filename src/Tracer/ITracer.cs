namespace DKX.Tracer
{
	public interface ITracer
	{
		ICurrentSpanProvider CurrentSpanProvider { get; }
		
		IRootSpan Start(string name);
	}
}