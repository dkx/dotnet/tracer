using System;
using System.Threading;
using System.Threading.Tasks;
using DKX.Tracer.Exporters;
using Microsoft.AspNetCore.Http;

namespace DKX.Tracer
{
	public class RootSpan : Span, IRootSpan
	{
		private readonly IExporter _exporter;
		
		public RootSpan(
			IExporter exporter,
			IDateTimeProvider dateTimeProvider,
			string name
		) : base(dateTimeProvider, name)
		{
			_exporter = exporter;
		}

		public HttpRequest? HttpRequest { get; private set; }
		
		public HttpResponse? HttpResponse { get; private set; }

		public Exception? Exception { get; private set; }

		public async ValueTask DisposeAsync()
		{
			Dispose();
			await Save();
		}

		public IRootSpan AttachHttpRequest(HttpRequest request)
		{
			if (Closed)
			{
				throw new Exception($"Can not attach http request to closed span {Name}");
			}

			HttpRequest = request;

			return this;
		}

		public IRootSpan AttachHttpResponse(HttpResponse response)
		{
			if (Closed)
			{
				throw new Exception($"Can not attach http response to closed span {Name}");
			}

			HttpResponse = response;

			return this;
		}

		public IRootSpan AttachException(Exception exception)
		{
			if (Closed)
			{
				throw new Exception($"Can not attach exception to closed span {Name}");
			}

			Exception = exception;

			return this;
		}

		public Task Save(CancellationToken cancellationToken = default)
		{
			return _exporter.Save(this, cancellationToken);
		}
	}
}
