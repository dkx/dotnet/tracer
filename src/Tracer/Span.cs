using System;
using System.Collections.Immutable;
using DKX.Tracer.Attributes;
using DKX.Tracer.Events;

namespace DKX.Tracer
{
	public class Span : ISpan
	{
		public event EventHandler<SpanForkedArgs>? OnForked;
		
		public event EventHandler<SpanClosedArgs>? OnClosed;
		
		private readonly IDateTimeProvider _dateTimeProvider;

		public Span(IDateTimeProvider dateTimeProvider, string name, ISpan? parent = null)
		{
			_dateTimeProvider = dateTimeProvider;
			Name = name;
			Parent = parent;
			StartedAt = _dateTimeProvider.GetUtcNow();
		}

		public string Name { get; }

		public DateTime StartedAt { get; }
		
		public DateTime? ClosedAt { get; private set; }
		
		public ISpan? Parent { get; }

		public IImmutableList<ISpan> Children { get; private set; } = ImmutableList<ISpan>.Empty;

		public IImmutableList<IAttribute> Attributes { get; private set; } = ImmutableList<IAttribute>.Empty;

		public bool Closed => ClosedAt != null;
		
		public virtual void Dispose()
		{
			if (Closed)
			{
				throw new Exception("Can not dispose of already disposed span");
			}
			
			ClosedAt = _dateTimeProvider.GetUtcNow();
			OnClosed?.Invoke(this, new SpanClosedArgs(this));

			OnForked = null;
			OnClosed = null;
		}

		public ISpan AddAttribute(string name, string value)
		{
			Attributes = Attributes.Add(new StringAttribute(name, value));
			return this;
		}

		public ISpan AddAttribute(string name, long value)
		{
			Attributes = Attributes.Add(new NumberAttribute(name, value));
			return this;
		}

		public ISpan AddAttribute(string name, bool value)
		{
			Attributes = Attributes.Add(new BooleanAttribute(name, value));
			return this;
		}

		public ISpan Fork(string name)
		{
			if (Closed)
			{
				throw new Exception("Can not fork already closed span");
			}
			
			var child = new Span(_dateTimeProvider, name, this);
			Children = Children.Add(child);
			OnForked?.Invoke(this, new SpanForkedArgs(this, child));

			return child;
		}
	}
}