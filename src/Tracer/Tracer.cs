﻿using System;
using DKX.Tracer.Exporters;

namespace DKX.Tracer
{
	public class Tracer : ITracer
	{
		private readonly IExporter _exporter;
		
		private readonly IDateTimeProvider _dateTimeProvider;

		private readonly CurrentSpanProvider _currentSpanProvider = new CurrentSpanProvider();

		public Tracer(IExporter exporter, IDateTimeProvider? dateTimeProvider = null)
		{
			_exporter = exporter;
			_dateTimeProvider = dateTimeProvider ?? new CurrentDateTimeProvider();
		}

		public ICurrentSpanProvider CurrentSpanProvider => _currentSpanProvider;
		
		public IRootSpan Start(string name)
		{
			if (_currentSpanProvider.CurrentSpan != null)
			{
				throw new Exception($"Can not start new trace, trace {_currentSpanProvider.CurrentSpan.Name} is still running");
			}
			
			var span = new RootSpan(_exporter, _dateTimeProvider, name);
			_currentSpanProvider.SetCurrentSpan(span);

			return span;
		}
	}
}
