using DKX.Tracer;
using Xunit;

namespace DKX.TestsTracer
{
	public class CurrentSpanProviderTest
	{
		private readonly CurrentSpanProvider _provider = new CurrentSpanProvider();
		
		[Fact]
		public void CurrentSpan_Null()
		{
			Assert.Null(_provider.CurrentSpan);
		}

		[Fact]
		public void CurrentSpan_Simple()
		{
			var root = CreateSpan("root");
			
			_provider.SetCurrentSpan(root);
			Assert.Same(root, _provider.CurrentSpan);

			var child = root.Fork("child");
			Assert.Same(child, _provider.CurrentSpan);
			
			child.Dispose();
			Assert.Same(root, _provider.CurrentSpan);
			
			root.Dispose();
			Assert.Null(_provider.CurrentSpan);
		}

		[Fact]
		public void CurrentSpan_Nested()
		{
			var root = CreateSpan("root");
			
			_provider.SetCurrentSpan(root);
			Assert.Same(root, _provider.CurrentSpan);

			var child = root.Fork("child");
			Assert.Same(child, _provider.CurrentSpan);

			var childInner1 = child.Fork("childInner1");
			Assert.Same(childInner1, _provider.CurrentSpan);

			var childInner2 = childInner1.Fork("childInner2");
			Assert.Same(childInner2, _provider.CurrentSpan);

			var childInner3 = childInner2.Fork("childInner3");
			Assert.Same(childInner3, _provider.CurrentSpan);
			
			childInner3.Dispose();
			Assert.Same(childInner2, _provider.CurrentSpan);
			
			childInner2.Dispose();
			Assert.Same(childInner1, _provider.CurrentSpan);
			
			childInner1.Dispose();
			Assert.Same(child, _provider.CurrentSpan);

			child.Dispose();
			Assert.Same(root, _provider.CurrentSpan);
			
			root.Dispose();
			Assert.Null(_provider.CurrentSpan);
		}

		private static ISpan CreateSpan(string name, ISpan? parent = null)
		{
			return new Span(new CurrentDateTimeProvider(), name, parent);
		}
	}
}
